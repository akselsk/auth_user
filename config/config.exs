use Mix.Config

config :simple_cqrs, :read_model_handlers, [AuthUser.ReadModel]

import_config "#{Mix.env()}.exs"
