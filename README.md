# AuthUser

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `auth_user` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:auth_user, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/auth_user](https://hexdocs.pm/auth_user).

