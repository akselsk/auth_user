defmodule AuthUser.MixProject do
  use Mix.Project

  def project do
    [
      app: :auth_user,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :simple_cqrs],
      mod: {AuthUser.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:simple_cqrs, git: "https://gitlab.com/akselsk/simple_cqrs"},
      #{:simple_cqrs, path: "../gigalixir_apps/simple_cqrs/"},
      {:simple_auth, git: "https://gitlab.com/akselsk/simple_auth" },
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_view, github: "phoenixframework/phoenix_live_view"},
      {:oauth2, "~> 2.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
