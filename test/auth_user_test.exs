defmodule AuthUserTest do
  use ExUnit.Case
  doctest AuthUser

  setup_all do
    on_exit(fn ->
      # File.rm_rf("read_model_db_test")
      File.rm_rf("stream_db_test")
    end)

    :ok
  end

  test "User read_model" do
    test_create = %AuthUser.Aggregate.Create{
      aggregate_id: "read_model_test",
      email: "aksel@msn.com",
      pwd: "etpwd"
    }

    assert AuthUser.Aggregate.dispatch(test_create) == :ok
    :timer.sleep(1000)

    assert AuthUser.ReadModel.get("read_model_test")
           |> (fn user -> SimpleAuth.verify_pass(user.pwd_hash, "etpwd") end).() ==
             {:ok, "authenticated"}
  end
end
