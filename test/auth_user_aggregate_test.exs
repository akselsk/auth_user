defmodule AggregatesTest do
  use ExUnit.Case

  setup_all do
    on_exit(fn ->
      File.rm_rf("read_model_db_test")
      File.rm_rf("stream_db_test")
    end)

    :ok
  end

  test "User aggregate" do
    test_create = %AuthUser.Aggregate.Create{
      aggregate_id: "test1",
      email: "aksel@msn.com",
      pwd: "etpwd"
    }

    bad_mail_create = %AuthUser.Aggregate.Create{
      test_create
      | aggregate_id: "test2",
        email: "aksel"
    }

    bad_pwd_create = %AuthUser.Aggregate.Create{test_create | aggregate_id: "test3", pwd: "asd"}
    assert AuthUser.Aggregate.dispatch(test_create) == :ok
    assert AuthUser.Aggregate.dispatch(test_create) == {:error, "already created"}
    assert AuthUser.Aggregate.dispatch(bad_mail_create) == {:error, "Must contain @"}

    assert AuthUser.Aggregate.dispatch(bad_pwd_create) ==
             {:error, "Password must be more than 8 characters"}
  end
end
