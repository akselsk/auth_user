defmodule AuthUser.GuestLive do
  use Phoenix.LiveView
  import SimpleAuth
  use Phoenix.HTML

  def mount(params, _session, socket) do
    {:ok,
     assign(socket,
       danger: nil,
       info: nil,
       login_page_state: %{user: %{"email" => "", "pwd" => ""}},
       register_page_state: %{
         user: %{"email" => "", "pwd" => "", "confirm_pwd" => ""},
         errors: []
       },
       page: Map.get(params, "page", "register")
     )}
  end

  def render(assigns) do
    ~L"""
      	<%= case @page do %>
      	<% "register" -> %> <%= register_page(@danger, @info, @register_page_state) %>
      	<% "login" -> %> <%=login_page(@danger, @info, @login_page_state) %>
      	<% end %>
    """
  end

  def handle_params(params, _session, socket) do
    new_socket = assign(socket,:danger,nil)
    {:noreply, assign(new_socket, page: Map.get(params, "page", "register"))}
  end

  def handle_event("set_page_login", _a, socket) do
    {:noreply, push_patch(socket, to: "/guest/login", replace: true)}
    end

  def handle_event("set_page_register", _a, socket),
    do: {:noreply, assign(socket, :page, "register")}

  def handle_event("login", login_form, socket),
    do: authenticate_user(login_form["user"], socket)

  def handle_event("validate_register_form", register_form, socket) do
    validate_register_form(register_form["user"], socket)
  end

  def handle_event("create_user", register_form, socket) do
    create_user(register_form["user"], socket)
  end


  def error_tag_custom(errors, field) when is_list(errors) and is_atom(field) do
    case Keyword.fetch(errors, field) do
      {:ok, message} -> content_tag(:span, message, class: "error_tag")
      :error -> html_escape("")
    end
  end

  def authenticate_user(user, socket) do
      IO.inspect "calling authenticate user"
    user["email"]
    |> AuthUser.ReadModel.get()
    |> (fn db_user -> Map.get(db_user, :pwd_hash) end).()
    |> case do
      nil ->
        {:noreply, assign(socket, :danger,  "User doesn't exist")}

      pwd_hash ->
        SimpleAuth.verify_pass(pwd_hash, user["pwd"])
        |> case do
          {:ok, "authenticated"} ->
            IO.inspect(user["email"])
            {:stop,
             socket
             |> Phoenix.LiveView.put_flash(:current_user_id, user["email"])
             |> redirect(to: "/user/profile")}

          {:error, reason} ->
            {:noreply, assign(socket, :danger, reason)}
        end
    end
  end

  def validate_register_form(user, socket) do
    update_errors = fn errors, validate_result, key ->
      case validate_result do
        {:ok, _} -> Keyword.delete(errors, key)
        {:error, reason} -> Keyword.put(errors, key, reason)
      end
    end

    validate_confirm_pwd = fn user ->
      if user["pwd"] == user["confirm_pwd"] do
        {:ok, :placeholder}
      else
        {:error, "Not equal"}
      end
    end

    errors =
      Map.fetch!(socket, :assigns)
      |> Map.fetch!(:register_page_state)
      |> Map.fetch!(:errors)
      |> update_errors.(validate_email(user["email"]), :email)
      |> update_errors.(validate_pwd(user["pwd"]), :pwd)
      |> update_errors.(validate_confirm_pwd.(user), :confirm_pwd)


    {:noreply,
     assign(socket, :register_page_state, %{
       socket.assigns.register_page_state
       | user: user,
         errors: errors
     })}
  end

  def create_user(user, socket) do
    %AuthUser.Aggregate.Create{aggregate_id: user["email"], email: user["email"], pwd: user["pwd"]}
    |> AuthUser.Aggregate.dispatch()
    |> case do
      :ok ->
        {:noreply, assign(socket, :page, "login") |> assign(:info, "User Sucessfully created")}

      {:error, reason} ->
        {:noreply, assign(socket, :danger, reason)}
    end
  end

  def register_page(danger, info, assigns) do
    ~L"""
    <div class="alert alert-danger" role="alert"><%= danger%></div>
    <form  phx-change="validate_register_form" phx-submit="create_user">
    	<%= unless @user["email"] == "" do error_tag_custom  @errors, :email end %>
    	<input type="email" name="user[email]" value="<%= @user["email"] %>" placeholder= Email:  phx-debounce="blur"/>
    	<%= unless @user["pwd"] == "" do error_tag_custom  @errors, :pwd end %>
    	<input type="password" name="user[pwd]"  value="<%= @user["pwd"] %>" placeholder=Password:  phx-debounce="blur"/>
    	<%= unless @user["confirm_pwd"] == "" do error_tag_custom  @errors, :confirm_pwd end%>
    	<input type="password" name="user[confirm_pwd]"  value="<%= @user["confirm_pwd"] %>" placeholder='Confirm Password':  phx-debounce="blur"/>
        <h2>By creating an account you agree to our <a> Terms & Privacy</a>.</h2>
    	<h2><b> Already have an account? </b> <a href='#' phx-click="set_page_login"> Sign in </a> </h2>
    	<input class="button_style button" type="submit" value="Sign Up" >
    </form>
    """
  end

  def login_page(danger,info, assigns) do
    ~L"""
        <style>
        .modal {
          position: fixed; /* Stay in place */
          z-index: 1; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal h1, h2 ,h3{
        	color: black;
        }
        .modal p{
        	color: rgba(0,0,0,0.6);
        	font-weight: 300;
        }
        .modal a{
        	color: rgba(0,0,0,0.6);
        	font-weight: 300;
        	text-decoration: underline;
        }


        /* Modal Content */
        .modal-content {
        text-align: center;
          position: relative;
          background-color: #fefefe;
          margin: auto;
          padding: 0;
          border: 1px solid #888;
          width: 80%;
          max-width: 600px;
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
          -webkit-animation-name: animatetop;
          -webkit-animation-duration: 1.5s;
          animation-name: animatetop;
          animation-duration: 1.5s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
          from {opacity:0}
          to {opacity:1}
        }

        @keyframes animatetop {
          from {opacity:0}
          to {opacity:1}
        }

        /* The Close Button */

        .modal-header {
          padding: 20px;
          color: black;
          align-content: center;
        }


        .modal-body {padding: 2px 16px;}

        .modal-footer {
          padding: 20px;
          display: flex;
          justify-content: space-around;
        }
        </style>
        <div class="modal">
          <div class="modal-content">
    	<div class="alert alert-danger" role="alert"><%= danger%></div>
    	<div class="alert alert-info" role="alert"><%= info%></div>
            <div class="modal-header">
              <h1> Welcome</h1>
            </div>
            <div class="modal-body">
            	<form  phx-submit="login">
            	<input type="email" name="user[email]" value="<%= @user["email"] %>" placeholder= Email:  phx-debounce="blur"/>
            	<input type="password" name="user[pwd]"  value="<%= @user["pwd"] %>" placeholder=Password:  phx-debounce="blur"/>
            	<input class="button_style button" type="submit" value="Login" >
            	</form>
            </div>
            <div class="modal-footer">

            <i class="fab fa-twitter"> </i>
            <i class="fab fa-google"></i>
            <i class="fab fa-facebook"></i>
            <i class="fab fa-github"></i>
            </div>
          <p>New user? <a href='#' phx-click="set_page_register"> Sign up</a> </p>
          </div>
          </div>
    """
  end


end
