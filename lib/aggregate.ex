defmodule AuthUser.Aggregate do
  @moduledoc """
      Example of an aggregate
  """
  defmodule State, do: defstruct([:aggregate_id, :email])

  defmodule Create, do: defstruct([:aggregate_id, :email, :pwd])
  defmodule Created, do: defstruct([:aggregate_id, :email, :pwd_hash, :event_id])

  import SimpleAuth

  def dispatch(%Create{} = cmd), do: SimpleCqrs.Aggregate.dispatch(cmd.aggregate_id,__MODULE__,cmd)

  #TODO be more strict on module naming
  alias AuthUser.Aggregate

  def execute(state, %Create{} = command) do
    if state do
      {:error, "already created"}
    else
      with {:ok, _email} <- validate_email(command.email),
           {:ok, _pwd} <- validate_pwd(command.pwd),
           hashed_pwd <- hash_pwd(command.pwd) do
        state
        |> apply_event(%Created{
          aggregate_id: command.aggregate_id,
          email: command.email,
          pwd_hash: hashed_pwd,
          event_id: UUID.uuid1()
        })
      else
        err -> err
      end
    end
  end

  def apply_event(_, %Created{} = event) do
    {%State{aggregate_id: event.aggregate_id, email: event.email}, event}
  end

end
