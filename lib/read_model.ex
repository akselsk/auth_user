defmodule AuthUser.ReadModel do
  defmodule State, do: defstruct([:id, :email, :pwd_hash])

  def get(id), do: SimpleCqrs.ReadModel.get_state(id, __MODULE__)

  def handle(%AuthUser.Aggregate.Created{} = event) do
    SimpleCqrs.ReadModel.dispatch(event.aggregate_id, __MODULE__, event)
  end

  #def handle(%AuthUser.Aggregate.OAuth2UserCreated{} = event), do: handle_in_read_model_process(event)
  def handle(_), do: :ok

  def apply_event(id, _, %AuthUser.Aggregate.Created{} = event) do
    %State{id: id, email: event.email, pwd_hash: event.pwd_hash}
  end

  #def apply_event(_, %AuthUser.Aggregate.OAuth2UserCreated{} = event) do
    #%State{aggregate_id: event.aggregate_id, access_token: event.access_token, refresh_token: event.pwd_hash}
  #end
end
