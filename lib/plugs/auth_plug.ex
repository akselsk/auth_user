
defmodule AuthUser.AuthPlug do
  import Plug.Conn
  import Phoenix.Controller

  def init(opts), do: opts

  def call(conn, opts) do
    conn
    |> get_session(:current_user_id)
    |> case do
      nil ->
        conn
        |> redirect(to: opts[:login_page])
        |> halt()

      _user -> conn
    end
  end

end
