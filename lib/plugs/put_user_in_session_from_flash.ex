defmodule AuthUser.FlashToSessionPlug do

  @valid_keys [:signing_salt]

  @behaviour Plug

  @cookie_key "__phoenix_flash__"

  @impl Plug
  def init(opts) do
    if Keyword.keys(opts) -- @valid_keys != [] do
      raise ArgumentError, """
      invalid options passed to #{inspect(__MODULE__)}.

      Valid options include #{inspect(@valid_keys)}, got: #{inspect(opts)}
      """
    end

    opts
  end

  @impl Plug
  def call(conn, opts) do

    case cookie_flash(conn, salt(conn, opts)) do
      {conn, nil} ->
        Phoenix.Controller.fetch_flash(conn, [])
      {conn, %{current_user_id: id} = _flash} ->
          case id do
              nil -> conn
              x -> conn
              	|> Plug.Conn.put_session("current_user_id", x)
        	|> Phoenix.Controller.fetch_flash([])
        	end
    end
  end

  defp cookie_flash(%Plug.Conn{req_cookies:  %{@cookie_key => token}} = conn, salt) do
    flash =
      case Phoenix.Token.verify(conn, salt, token, max_age: 60_000) do
        {:ok, %{} = flash} -> flash
        _ -> nil
      end

    {Plug.Conn.delete_resp_cookie(conn, @cookie_key), flash}
  end

  defp cookie_flash(%Plug.Conn{} = conn, _salt), do: {conn, nil}

  defp salt(conn, opts) do
    endpoint = Phoenix.Controller.endpoint_module(conn)
    opts[:signing_salt] || Phoenix.LiveView.Utils.salt!(endpoint)
  end

  @doc false
  def sign(endpoint_mod, salt, %{} = flash) do
    Phoenix.Token.sign(endpoint_mod, salt, flash)
  end
end
