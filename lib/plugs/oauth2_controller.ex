defmodule AuthUser.Oauth2Controller do
  import Phoenix.Controller
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, opts) do
      IO.puts "conn"
      # get path params and etc
      Plug.Conn
      if conn.request_path =~ "callback" do
          callback(conn,conn.params,conn.path_params["provider"],opts[:redirect_route])
      else
      	redirect(conn, external: authorize_url!(conn.path_params["provider"]))
      end

      #redirect(conn,external: "https://google.com")
  end

  def index(conn, %{}) do
      redirect(conn, external: "https://google.com")
  end

  defp oura_client do
    OAuth2.Client.new(
      # default
      strategy: OAuth2.Strategy.AuthCode,
      # Must make these into runtime variables
      client_id: System.get_env("OURA_CLIENT"),
      client_secret: System.get_env("OURA_SECRET"),
      redirect_uri: "http://localhost:4000/auth/oura/callback",
      state: "XXX",
      authorize_url: "https://cloud.ouraring.com/oauth/authorize",
      token_url: "https://api.ouraring.com/oauth/token"
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  def authorize_url!("oura") do
      OAuth2.Client.authorize_url!(oura_client())
  end

  def callback(conn,params, "oura", redirect_route) do
      IO.inspect "in callback"
      IO.inspect conn
      IO.inspect params
      code = params["code"]
      IO.puts "code is"
      IO.inspect code
      # Do not need to make a user..

      client = OAuth2.Client.get_token!(oura_client(),[code: code], [], [])
      IO.puts "client is"
      IO.inspect client
      ## This call can be made from the main client. Also because it may be updated
      user = OAuth2.Client.get!(client, "https://api.ouraring.com/v1/userinfo").body
      # be sure to put a user id... Oura is different as data is initiallly stored there

      conn
      |> put_session(:current_user_id, user["user_id"])
      |> put_session(:access_token, client.token.access_token)
      |> put_session(:refresh_token,client.token.refresh_token)

      #Config this url
      |> redirect(to: redirect_route)
      end


  end
